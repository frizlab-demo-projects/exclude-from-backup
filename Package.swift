// swift-tools-version:5.5
import PackageDescription



let package = Package(
	name: "ExcludeFromBackup",
	platforms: [
		.macOS(.v11)
	],
	products: [.executable(name: "ExcludeFromBackup", targets: ["ExcludeFromBackup"])],
	dependencies: [
		.package(url: "https://github.com/apple/swift-argument-parser.git", from: "1.0.1"),
		.package(url: "https://github.com/xcode-actions/clt-logger.git",    from: "0.3.4")
	],
	targets: [
		.executableTarget(name: "ExcludeFromBackup", dependencies: [
			.product(name: "ArgumentParser", package: "swift-argument-parser"),
			.product(name: "CLTLogger",      package: "clt-logger")
		])
	]
)
