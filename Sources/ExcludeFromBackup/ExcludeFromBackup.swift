import Foundation

import ArgumentParser
import CLTLogger
import Logging



@main
struct ExcludeFromBackup : ParsableCommand {
	
	struct InternalError : Error {
		var message: String
	}
	
	@Flag
	var verbose = false
	
	@Argument
	var excludedPaths: [String]
	
	func run() throws {
		LoggingSystem.bootstrap{ _ in CLTLogger() }
		let logger = { () -> Logger in
			var ret = Logger(label: "me.frizlab.exclude-from-backup")
			ret.logLevel = verbose ? .debug : .info
			return ret
		}()
		
		guard #available(macOS 11.3, *) else {
			logger.error("Excluding item from sync is not available before macOS 11.3")
			return
		}
		
		for p in excludedPaths {
			do {
				logger.debug("Treating path \(p)")
				var url = URL(fileURLWithPath: p)
				/* Interesting keys, but do not work apparently: .ubiquitousItemIsExcludedFromSyncKey, .isPurgeableKey*/
				let rv = try url.resourceValues(forKeys: [.isExcludedFromBackupKey])
				guard let excluded = rv.isExcludedFromBackup else {
					throw InternalError(message: "isExcludedFromBackup is nil, but requested")
				}
				if excluded {
					logger.debug("Path already excluded, doing nothing: \(p)")
				} else {
					logger.info("Excluding \(p)")
					var newRv = rv
					newRv.isExcludedFromBackup = true
					try url.setResourceValues(newRv)
				}
			} catch {
				logger.error("Error treating path \(p): \(error)")
			}
		}
	}
	
}
